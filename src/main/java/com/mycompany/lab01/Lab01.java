/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;


import java.util.Scanner;
public class Lab01 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        System.out.println("Welcome OX");
        char[][] gameBoard = {{'1',' ','2',' ','3'},
                              {'4',' ','5',' ','6'},
                              {'7',' ','8',' ','9'}};
       
        while(true){         
        //o turn
        printboard(gameBoard);
        placeO(gameBoard);
        //checktie
        boolean tie = checktie(gameBoard);
        if (tie == true){
            System.out.println("draw");
            break;
        }
        //check winning
        String result = winOX(gameBoard);
        if(result.equals("X win")|| result.equals("O win")){
            printboard(gameBoard);
            System.out.println(result);
            System.out.println("draw");
            break;
        }
        
        //X turn
        printboard(gameBoard);
        placeX(gameBoard);
        tie = checktie(gameBoard);
        if (tie == true){
            break;
        }
        result = winOX(gameBoard);
        if(result.equals("X win")|| result.equals("O win")){
            printboard(gameBoard);
            System.out.println(result);
            break;
        }
        
        } 
        

    }
  static void printboard(char[][]gameBoard){
        for(char[] row: gameBoard){
            for(char Col : row){   
                System.out.print(Col);
            }
            System.out.println("");
        }
    }
   static void placeO(char[][]gameBoard){
            Scanner kb = new Scanner(System.in);

            System.out.println("O turn");
            System.out.print("Please input number:");
            int playerposition = kb.nextInt();
            if (playerposition == 1) {
                gameBoard[0][0] = 'O';
            }else if (playerposition == 2) {
                gameBoard[0][2] = 'O';
            }else if (playerposition == 3) {
                gameBoard[0][4] = 'O';
            }else if (playerposition == 4) {
                gameBoard[1][0] = 'O';
            }else if (playerposition == 5) {
                gameBoard[1][2] = 'O';
            }else if (playerposition == 6) {
                gameBoard[1][4] = 'O';
            }else if (playerposition == 7) {
                gameBoard[2][0] = 'O';
            }else if (playerposition == 8) {
                gameBoard[2][2] = 'O';
            }else if (playerposition == 9) {
                gameBoard[2][4] = 'O';
            }
   }
   static void placeX(char[][]gameBoard){
                   Scanner kb = new Scanner(System.in);

            System.out.println("X turn");
            System.out.print("Please input number:");
            int playerposition = kb.nextInt();
            if (playerposition == 1) {
                gameBoard[0][0] = 'X';
            }else if (playerposition == 2) {
                gameBoard[0][2] = 'X';
            }else if (playerposition == 3) {
                gameBoard[0][4] = 'X';
            }else if (playerposition == 4) {
                gameBoard[1][0] = 'X';
            }else if (playerposition == 5) {
                gameBoard[1][2] = 'X';
            }else if (playerposition == 6) {
                gameBoard[1][4] = 'X';
            }else if (playerposition == 7) {
                gameBoard[2][0] = 'X';
            }else if (playerposition == 8) {
                gameBoard[2][2] = 'X';
            }else if (playerposition == 9) {
                gameBoard[2][4] = 'X';
            }
           
   }
   static String winOX(char[][]gameBoard){
      
       if (gameBoard[0][0]==gameBoard[0][2]&&gameBoard[0][4]==gameBoard[0][2]){
           return gameBoard[0][0]+" win";
       }else if(gameBoard[1][0]==gameBoard[1][2]&&gameBoard[1][4]==gameBoard[1][2]){
           return gameBoard[1][0]+" win";
       }else if(gameBoard[2][0]==gameBoard[2][2]&&gameBoard[2][4]==gameBoard[2][2]){
           return gameBoard[2][0]+" win";
       }else if(gameBoard[0][0]==gameBoard[1][0]&&gameBoard[2][0]==gameBoard[1][0]){
           return gameBoard[0][0]+" win";
       }else if(gameBoard[0][2]==gameBoard[1][2]&&gameBoard[2][2]==gameBoard[1][2]){
           return gameBoard[0][2]+" win";
       }else if(gameBoard[0][4]==gameBoard[1][4]&&gameBoard[2][4]==gameBoard[1][4]){
           return gameBoard[0][4]+" win";
       }else if(gameBoard[0][0]==gameBoard[1][2]&&gameBoard[2][4]==gameBoard[1][2]){
           return gameBoard[0][0]+" win";
       }else if(gameBoard[0][4]==gameBoard[1][2]&&gameBoard[2][0]==gameBoard[1][2]){
           return gameBoard[0][4]+" win";
       }   
           
           return"";
        
   }
     static boolean checktie(char[][]gameBoard){
        boolean Tie = true;
        for (char[] row :gameBoard){
            for(char col : row){
                if(Character.isDigit(col)){
                    Tie = false;
                    break;
                }
            }
        }
        return Tie;
     }
}
